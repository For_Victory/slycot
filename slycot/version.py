
# THIS FILE IS GENERATED FROM SLYCOT SETUP.PY
short_version = '0.3.0'
version = '0.3.0'
full_version = '0.3.0'
git_revision = '3c7b91b5dcd3664b9f9acec184b86d246310dc76'
release = True

if not release:
    version = full_version
